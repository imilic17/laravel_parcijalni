<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Blog;
use App\Models\Omeni;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

       // User::factory()->create([
       //     'name' => 'Test User',
       //    'email' => 'test@example.com',
       // ]);
       Omeni::factory(3)->create();
       Blog::factory(9)->create();
    }
}
