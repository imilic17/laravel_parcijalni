<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\User;

class UserController extends Controller
{
    public function registration()
    {
        return view('users.registration');
    }

    public function registerUser(Request $request)
    {
        $formFileds=$request->validate([
            'name'=>'required',
            'email'=> ['required', 'email', Rule::unique('users', 'email')],
            'password' => 'required|confirmed'
        ]);

        $formFileds['password'] = bcrypt($formFileds['password']);

        $user = User::create($formFileds);

        //login
        auth()->login($user);

        return redirect('/');
    }

    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function login()
    {
        return view('users.login');
    }

    public function authentication(Request $request)
    {
        $formFields = $request->validate([
            'email' => ['required', 'email'],
            'password' => 'required'
        ]);

        if (auth()->attempt($formFields))
        {
            $request->session()->regenerate();
            return redirect('/');
        }

        return back();
    }
}
