<?php

namespace App\Http\Controllers;

use App\Models\Omeni;
use Illuminate\Http\Request;

class OmeniController extends Controller
{
    public function omeni()
    {
        return view('omeni',
    [
        'omenis' =>Omeni::all()
    ]);
    }

    public function updateomeni($id)
    {
        $omeni = Omeni::find($id);
        return view('updateomeni',
    [
        'omeni' => $omeni
    ]);
    }

    public function editomeni(Request $request, $id)
    {
        $omeni = Omeni::find($id);

        $omeni['opis'] = $request->opis;

        $omeni->save();
        return redirect('/omeni');
    }
}
