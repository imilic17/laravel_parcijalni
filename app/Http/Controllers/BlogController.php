<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return view('blog.blog',
        [
            'blogs' => Blog::all()
        ]);
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(Request $request)
    {
        $formFields = $request->validate([
            'title'=>'required',
            'subtitle'=>'required',
            'content'=>'required'
        ]);
        Blog::create($formFields);

        return redirect('/');
    }

    public function update($id)
    {
        $blog = Blog::find($id);
        return view('blog.update',
    [
        'blog' =>$blog
    ]);
    }

    public function edit(Request $request, $id)
    {
        $blog = Blog::find($id);

        $blog['title'] = $request->title;
        $blog['subtitle'] = $request->subtitle;
        $blog['content'] = $request->content;

        $blog->save();
        return redirect('/');

    }

    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect('/');
    }
}
