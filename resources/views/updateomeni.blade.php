<x-layout>
    <div class="row">
        <div class=mb-3>
            <h2>Izmjena podataka o meni</h2>
        </div>
        <form method="POST" action="/omenis/{{$omeni->id}}">
            @csrf
            @method("PUT")
        <div class="mb-3">
            <input type="text" name="opis" value="{{$omeni->opis}}" class="form-control" id="exampleTitle" aria-describedby="titleHelp">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
</div>
</x-layout>