<x-layout>
  @auth
    <div class="row">
        <div class="col-sm-2">
        <a class="btn btn-primary" href="/create"> Dodaj novi blog post</a>
        </div>
    </div>
    @endauth
<div class="row row-cols-3">
    @foreach ($blogs as $key=>$blog)
    <div class="col">
      <b>{{$blog->title}} </b><br>
      <i>{{$blog->subtitle}}</i><br><br>
      {{$blog->content}}
      <br><br>
      @auth
      <a class="btn btn-primary" href="/update/{{$blog->id}}">Uredi post</a>
      <form method="POST" action="/blogs/{{$blog->id}}">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Izbriši post</button>
      </form>
      @endauth
    </div>
    @endforeach
  </div>
</x-layout>