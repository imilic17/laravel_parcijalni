<x-layout>
    <div class="row">
        <div class=mb-3>
            <h2>Unesi novi blog post</h2>
        </div>
        <form method="POST" action="/blogs">
            @csrf
        <div class="mb-3">
            <label for="exampleTitle" class="form-label">Naslov </label>
            <input type="text" name="title" class="form-control" id="exampleTitle" aria-describedby="titleHelp">
        </div>
        </div>
        <div class="mb-3">
        <label for="exampleTitle" class="form-label">Podnaslov </label>
            <input type="text" name="subtitle" class="form-control" id="exampleTitle" aria-describedby="titleHelp">
        </div>
        <div class="mb-3">
        <label for="exampleTitle" class="form-label"> Sadržaj </label>
            <input type="text" name="content" class="form-control" id="exampleTitle" aria-describedby="titleHelp">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
</div>
</x-layout>