<x-layout>
    <h1> O meni </h1>
    @foreach ($omenis as $key=>$omeni)
    <p> {{$omeni->opis}}
        @auth
        <a class="btn btn-primary" href="/updateomeni/{{$omeni->id}}"> Uredi podatke</a>
        @endauth
    </p>
    @endforeach
</x-layout>
