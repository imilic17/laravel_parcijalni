<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\OmeniController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', [BlogController::class, 'index']);
Route::get('/omeni', [OmeniController::class, 'omeni']);
Route::get('/create', [BlogController::class, 'create'])->middleware('auth');
Route::get('/update/{id}', [BlogController::class, 'update'])->middleware('auth');
Route::post('/blogs', [BlogController::class, 'store'])->middleware('auth');
Route::put('/blogs/{id}', [BlogController::class, 'edit'])->middleware('auth');
Route::delete('/blogs/{id}', [BlogController::class, 'destroy'])->middleware('auth');

Route::get('/updateomeni/{id}', [OmeniController::class, 'updateomeni'])->middleware('auth');
Route::put('/omenis/{id}', [OmeniController::class, 'editomeni'])->middleware('auth');

Route::get('/registration', [UserController::class, 'registration']);
Route::get('/login', [UserController::class, 'login']);
Route::post('/user_registration', [UserController::class, 'registerUser']);
Route::post('/authentication', [UserController::class, 'authentication']);
Route::post('/logout', [UserController::class, 'logout'])->middleware('auth');